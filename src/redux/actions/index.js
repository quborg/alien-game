import { MOVE_OBJECTS, START_GAME } from "@/utils/constants";

export const moveObjects = (mousePosition) => ({
  type: MOVE_OBJECTS,
  mousePosition,
});

export const startGame = () => ({
  type: START_GAME,
});
