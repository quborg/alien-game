import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

import { getMousePosition } from "@/utils/helper";
import Game from "@/react/components/Game";
import { moveObjects } from "@/redux/actions";

class App extends Component {
  trackMouse(event) {
    const mousePosition = getMousePosition(event);
    this.mousePosition = mousePosition;
    this.props.moveObjects(mousePosition);
  }

  render() {
    return (
      <Game
        angle={this.props.angle}
        gameState={this.props.gameState}
        startGame={this.props.startGame}
        trackMouse={(event) => this.trackMouse(event)}
      />
    );
  }
}

App.propTypes = {
  angle: PropTypes.number.isRequired,
  gameState: PropTypes.shape({
    started: PropTypes.bool.isRequired,
    kills: PropTypes.number.isRequired,
    lives: PropTypes.number.isRequired,
  }).isRequired,
  moveObjects: PropTypes.func.isRequired,
  startGame: PropTypes.func.isRequired,
};

const mapState = (state) => ({
  angle: state.angle,
  gameState: state.gameState,
});

const mapDispatch = (dispatch) => ({
  moveObjects: (mousePosition) => {
    dispatch(moveObjects(mousePosition));
  },
});

export default connect(mapState, mapDispatch)(App);
