import React from "react";
import { pathFromBezierCurve } from "@/utils/helper";

const Title = () => {
  const textStyle = {
    fontFamily: '"Joti One", cursive',
    fontSize: 80,
    fill: "#cbca62",
  };

  const aliensLineCurve = {
    initialAxis: {
      x: -120,
      y: -460,
    },
    initialControlPoint: {
      x: 55,
      y: -60,
    },
    endingControlPoint: {
      x: 165,
      y: -60,
    },
    endingAxis: {
      x: 250,
      y: 0,
    },
  };

  const goHomeLineCurve = {
    ...aliensLineCurve,
    initialAxis: {
      x: -160,
      y: -370,
    },
    initialControlPoint: {
      x: 65,
      y: -100,
    },
    endingControlPoint: {
      x: 205,
      y: -50,
    },
    endingAxis: {
      x: 300,
      y: 0,
    },
  };

  return (
    <g filter="url(#shadow)">
      <defs>
        <path id="AliensPath" d={pathFromBezierCurve(aliensLineCurve)} />
        <path id="GoHomePath" d={pathFromBezierCurve(goHomeLineCurve)} />
      </defs>
      <text {...textStyle}>
        <textPath xlinkHref="#AliensPath">Aliens,</textPath>
      </text>
      <text {...textStyle}>
        <textPath xlinkHref="#GoHomePath">Go Home!</textPath>
      </text>
    </g>
  );
};

export default Title;
