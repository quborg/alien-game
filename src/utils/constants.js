export const MOVE_OBJECTS = "MOVE_OBJECTS";

export const START_GAME = 'START_GAME';

export const gameWidth = 800;

export const createInterval = 1000;

export const maxFlyingObjects = 4;

export const flyingObjectsStarterYAxis = -1000;

export const flyingObjectsStarterPositions = [
  -300,
  -150,
  150,
  300,
];